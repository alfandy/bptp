<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    

    <link href="{{ mix('/css/app.css') }}" rel="stylesheet" />
    
    {{-- <script src="{{mix('/js/manifest.js')}}" defer></script>
    <script src="{{mix('/js/vendor.js')}}" defer></script> --}}
    <script src="{{ mix('/js/app.js') }}" defer></script>
    <script data-ad-client="ca-pub-5636141414680878" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script async custom-element="amp-ad" src="https://cdn.ampproject.org/v0/amp-ad-0.1.js"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/slider2.css">
    <link rel="shortcut icon" href="{{ asset('assets') }}/img/icons/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('assets') }}/img/icons/114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('assets') }}/img/icons/72x72.png">
    <link rel="apple-touch-icon-precomposed" href="img{{ asset('assets') }}/icons/default.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900" rel="stylesheet">
    {{-- <link href="{{ asset('assets') }}/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet"> --}}
    <link href="{{ asset('assets') }}/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset('assets') }}/lib/owlcarousel/owl.carousel.min.css" rel="stylesheet">
    <link href="{{ asset('assets') }}/lib/owlcarousel/owl.theme.min.css" rel="stylesheet">
    <link href="{{ asset('assets') }}/lib/owlcarousel/owl.transitions.min.css" rel="stylesheet">
    {{-- <link href="{{ asset('assets') }}/css/style.css" rel="stylesheet"> --}}
    <link href="{{ mix('/assets/css/mix.css') }}" rel="stylesheet" />
    {{-- <link href="{{ asset('assets') }}/css/table.css" rel="stylesheet">
    <link href="{{ asset('assets') }}/css/slider2.css" rel="stylesheet"> --}}
    {{-- <link href="#" id="colour-scheme" rel="stylesheet"> --}}
  </head>
  <body class="page-index has-hero">
    @inertia

    <div id="divadsensedisplaynone" style="display:none;">
      <amp-ad width="100vw" height="320"
           type="adsense"
           data-ad-client="ca-pub-5636141414680878"
           data-ad-slot="2771706913"
           data-auto-format="rspv"
           data-full-width="">
        <div overflow=""></div>
      </amp-ad>
    </div>

  <script src="{{ asset('assets') }}/lib/jquery/jquery.min.js"></script>
  <script src="{{ asset('assets') }}/lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="{{ asset('assets') }}/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="{{ asset('assets') }}/lib/stellar/stellar.min.js"></script>
  <script src="{{ asset('assets') }}/lib/waypoints/waypoints.min.js"></script>
  <script src="{{ asset('assets') }}/lib/counterup/counterup.min.js"></script>
  <script src="{{ asset('assets') }}/contactform/contactform.js"></script>
  {{-- <script src="{{ asset('assets') }}/js/custom.js"></script> --}}
  {{-- <script src="{{ asset('assets') }}/js/color-switcher.js"></script> --}}
  <script src="{{ asset('assets') }}/contactform/contactform.js"></script>
  
  </body>
</html>