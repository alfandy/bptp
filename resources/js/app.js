

// require('./bootstrap');
import { InertiaApp } from '@inertiajs/inertia-vue'
import Vue from 'vue'

import moment from 'moment';
import VueMoment from 'vue-moment';
import 'vue-orgchart/dist/style.min.css'
import { BootstrapVue} from 'bootstrap-vue'
import Meta from 'vue-meta';

// Load Locales ('en' comes loaded by default)
require('moment/locale/id');

// Choose Locale
moment.locale('id');

Vue.use(BootstrapVue);
Vue.use(VueMoment, { moment });
Vue.use(Meta);
Vue.use(InertiaApp);


const app = document.getElementById('app')

new Vue({
  render: h => h(InertiaApp, {
    props: {
      initialPage: JSON.parse(app.dataset.page),
      resolveComponent: name => import(`./Pages/${name}`).then(module => module.default),
    },
  }),
}).$mount(app)