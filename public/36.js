(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[36],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Front/VisiMisi.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Front/VisiMisi.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../Shared/Layout */ "./resources/js/Shared/Layout.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Layout: _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: ['title', 'data']
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Front/VisiMisi.vue?vue&type=template&id=71bc0b8a&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Front/VisiMisi.vue?vue&type=template&id=71bc0b8a& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("Layout", { attrs: { title: _vm.title } }, [
    _c("div", { staticClass: "block block-border-bottom-grey block-pd-sm" }, [
      _c("div", { staticClass: "page-header" }, [
        _c("h2", { staticClass: "block-title" }, [_vm._v(_vm._s(_vm.title))])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12" }, [
        _c(
          "div",
          { staticClass: "block block-border-bottom-grey block-pd-sm" },
          [
            _vm.data
              ? _c("div", [
                  _c("h3", { staticClass: "block-title" }, [
                    _vm._v("\n\t\t\t\t\t\tVisi\n\t\t\t\t\t")
                  ]),
                  _vm._v(" "),
                  _c("p", { domProps: { innerHTML: _vm._s(_vm.data.visi) } }),
                  _vm._v(" "),
                  _c("h3", { staticClass: "block-title" }, [
                    _vm._v("\n\t\t\t\t\t\tMisi\n\t\t\t\t\t")
                  ]),
                  _vm._v(" "),
                  _c("p", { domProps: { innerHTML: _vm._s(_vm.data.misi) } })
                ])
              : _c("div", [
                  _c("h3", { staticClass: "text-center" }, [
                    _vm._v(
                      "\n\t\t\t\t\t\tMaaf Data Visi Misi Anda Belum Di Perbaruhi\n\t\t\t\t\t"
                    )
                  ])
                ])
          ]
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "span12", attrs: { id: "sp-breadcrumb" } }, [
      _c("ul", { staticClass: "breadcrumb " }, [
        _c(
          "li",
          [_c("inertia-link", { attrs: { href: "/" } }, [_vm._v("Home")])],
          1
        ),
        _vm._v(" "),
        _c("li", [_c("span", [_vm._v(_vm._s(_vm.title))])])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/Pages/Front/VisiMisi.vue":
/*!***********************************************!*\
  !*** ./resources/js/Pages/Front/VisiMisi.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _VisiMisi_vue_vue_type_template_id_71bc0b8a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./VisiMisi.vue?vue&type=template&id=71bc0b8a& */ "./resources/js/Pages/Front/VisiMisi.vue?vue&type=template&id=71bc0b8a&");
/* harmony import */ var _VisiMisi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./VisiMisi.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Front/VisiMisi.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _VisiMisi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _VisiMisi_vue_vue_type_template_id_71bc0b8a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _VisiMisi_vue_vue_type_template_id_71bc0b8a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Front/VisiMisi.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Front/VisiMisi.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/Pages/Front/VisiMisi.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VisiMisi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./VisiMisi.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Front/VisiMisi.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VisiMisi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Front/VisiMisi.vue?vue&type=template&id=71bc0b8a&":
/*!******************************************************************************!*\
  !*** ./resources/js/Pages/Front/VisiMisi.vue?vue&type=template&id=71bc0b8a& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VisiMisi_vue_vue_type_template_id_71bc0b8a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./VisiMisi.vue?vue&type=template&id=71bc0b8a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Front/VisiMisi.vue?vue&type=template&id=71bc0b8a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VisiMisi_vue_vue_type_template_id_71bc0b8a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VisiMisi_vue_vue_type_template_id_71bc0b8a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);