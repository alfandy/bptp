(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[27],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Post/Show.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Post/Show.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../Shared/Layout */ "./resources/js/Shared/Layout.vue");
/* harmony import */ var _Shared_Berita__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../Shared/Berita */ "./resources/js/Shared/Berita.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['post', 'domain', 'posts'],
  components: {
    Layout: _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__["default"],
    Berita: _Shared_Berita__WEBPACK_IMPORTED_MODULE_1__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Berita.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Shared/Berita.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_carousel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-carousel */ "./node_modules/vue-carousel/dist/vue-carousel.min.js");
/* harmony import */ var vue_carousel__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_carousel__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// import carousel from 'vue-owl-carousel2'

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['posts', 'domain', 'title'],
  components: {
    Carousel: vue_carousel__WEBPACK_IMPORTED_MODULE_0__["Carousel"],
    Slide: vue_carousel__WEBPACK_IMPORTED_MODULE_0__["Slide"]
  },
  data: function data() {
    return {
      itemBerita: {
        paddingLeft: '4px',
        paddingRight: '4px'
      },
      imageBerita: {
        objectFit: 'cover',
        width: '277px',
        height: '152px',
        marginLeft: 'auto',
        marginRight: 'auto'
      },
      mfpDateAuthor: {
        fontSize: "80%",
        lineHeight: "120%",
        color: "#666",
        marginBottom: "10px",
        display: "block"
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Post/Show.vue?vue&type=template&id=c283d45a&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Post/Show.vue?vue&type=template&id=c283d45a& ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.post
    ? _c(
        "Layout",
        { attrs: { title: _vm.post.title } },
        [
          _c(
            "div",
            { staticClass: "block block-border-bottom-grey block-pd-sm" },
            [
              _c("div", { staticClass: "page-header" }, [
                _c("h2", { staticClass: "block-title" }, [
                  _vm._v(_vm._s(_vm.post.title))
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "entry-meta muted clearfix" }, [
                _c(
                  "span",
                  { staticClass: "category-name" },
                  [
                    _vm._v("\n\t\t\t\tCategory: "),
                    _c(
                      "inertia-link",
                      {
                        attrs: {
                          href:
                            "/post/" + _vm.post.categorie.slug + "/categorie"
                        }
                      },
                      [_vm._v(_vm._s(_vm.post.categorie.name))]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "time",
                  {
                    staticClass: "create-date",
                    attrs: { datetime: _vm.post.created_at }
                  },
                  [
                    _vm._v(
                      "\n\t\t\t\tCreated: " +
                        _vm._s(
                          _vm.$moment(_vm.post.created_at).format("LLLL")
                        ) +
                        "\t\n\t\t\t"
                    )
                  ]
                ),
                _vm._v(" "),
                _c("span", { staticClass: "by-author" }, [
                  _vm._v(" \n\t\t\t\tWritten by "),
                  _c("span", { staticClass: "author vcard" }, [
                    _c("span", { staticClass: "fn n" }, [
                      _vm._v(_vm._s(_vm.post.author.name))
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-12" }, [
                _c(
                  "div",
                  { staticClass: "block block-border-bottom-grey block-pd-sm" },
                  [
                    _c("img", {
                      attrs: {
                        width: "100%",
                        src: _vm.domain + "/" + _vm.post.storege,
                        alt: _vm.post.title
                      }
                    })
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "block block-border-bottom-grey block-pd-sm" },
                  [_c("p", { domProps: { innerHTML: _vm._s(_vm.post.body) } })]
                )
              ])
            ]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "span12", attrs: { id: "sp-breadcrumb" } }, [
            _c("ul", { staticClass: "breadcrumb " }, [
              _c(
                "li",
                [
                  _c("inertia-link", { attrs: { href: "/" } }, [_vm._v("Home")])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "li",
                [
                  _c(
                    "inertia-link",
                    {
                      staticClass: "pathway",
                      attrs: {
                        href: "/post/" + _vm.post.categorie.slug + "/categorie"
                      }
                    },
                    [_vm._v(_vm._s(_vm.post.categorie.name))]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("li", [_c("span", [_vm._v(_vm._s(_vm.post.title))])])
            ])
          ]),
          _vm._v(" "),
          _c("Berita", {
            attrs: {
              posts: _vm.posts,
              domain: _vm.domain,
              title: "Berita Lainya"
            }
          })
        ],
        1
      )
    : _c(
        "Layout",
        { attrs: { title: "Halaman Tidak Ditemukan" } },
        [
          _c("h2", { staticClass: "text-center" }, [
            _c("b", [_vm._v("Halaman Tidak Ditemukan")])
          ]),
          _vm._v(" "),
          _c("Berita", {
            attrs: {
              posts: _vm.posts,
              domain: _vm.domain,
              title: "Berita Lainya"
            }
          })
        ],
        1
      )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Berita.vue?vue&type=template&id=1ad81c83&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Shared/Berita.vue?vue&type=template&id=1ad81c83& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "mission  block block-pd-sm block-bg-noise" },
      [
        _c("h2", { staticClass: "container block-title" }, [
          _vm._v("\n\t\t\t" + _vm._s(_vm.title) + "\n\t\t")
        ]),
        _vm._v(" "),
        _c(
          "Carousel",
          {
            attrs: {
              perPageCustom: [
                [320, 1],
                [425, 2],
                [768, 3],
                [1024, 4]
              ],
              autoplay: true,
              autoplayHoverPause: true,
              scrollPerPage: true,
              paginationEnabled: false
            }
          },
          _vm._l(_vm.posts.data, function(post, key) {
            return _c(
              "Slide",
              { key: post.id, style: _vm.itemBerita },
              [
                _c(
                  "inertia-link",
                  {
                    staticClass: "overlay-wrapper",
                    attrs: { href: "/post/" + post.slug }
                  },
                  [
                    _c("img", {
                      staticClass: "img-responsive underlay",
                      style: _vm.imageBerita,
                      attrs: {
                        src:
                          _vm.domain +
                          "/" +
                          post.storege.split(".")[0] +
                          "-cropped." +
                          post.storege.split(".")[1],
                        alt: post.title
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "overlay" }, [
                      _c("span", { staticClass: "overlay-content" }, [
                        _c("span", { staticClass: "h4" }, [_vm._v("Lihat")])
                      ])
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "item-details bg-noise" },
                  [
                    _c(
                      "h4",
                      { staticClass: "item-title" },
                      [
                        _c(
                          "inertia-link",
                          { attrs: { href: "/post/" + post.slug } },
                          [_vm._v(_vm._s(post.title))]
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("span", { style: _vm.mfpDateAuthor }, [
                      _vm._v(
                        _vm._s(_vm.$moment(post.created_at).format("LLLL")) +
                          " - " +
                          _vm._s(post.author.name)
                      )
                    ]),
                    _vm._v(" "),
                    _c("p", [
                      _vm._v(
                        _vm._s(
                          post.body.replace(/(<([^>]+)>)/gi, "").substr(0, 250)
                        )
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "inertia-link",
                      {
                        staticClass: "btn btn-more",
                        attrs: { href: "/post/" + post.slug }
                      },
                      [
                        _c("i", { staticClass: "fa fa-plus" }),
                        _vm._v("Lihat Lebih Lanjut")
                      ]
                    )
                  ],
                  1
                )
              ],
              1
            )
          }),
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/Pages/Post/Show.vue":
/*!******************************************!*\
  !*** ./resources/js/Pages/Post/Show.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Show_vue_vue_type_template_id_c283d45a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Show.vue?vue&type=template&id=c283d45a& */ "./resources/js/Pages/Post/Show.vue?vue&type=template&id=c283d45a&");
/* harmony import */ var _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Show.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Post/Show.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Show_vue_vue_type_template_id_c283d45a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Show_vue_vue_type_template_id_c283d45a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Post/Show.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Post/Show.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/Pages/Post/Show.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Show.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Post/Show.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Post/Show.vue?vue&type=template&id=c283d45a&":
/*!*************************************************************************!*\
  !*** ./resources/js/Pages/Post/Show.vue?vue&type=template&id=c283d45a& ***!
  \*************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_c283d45a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Show.vue?vue&type=template&id=c283d45a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Post/Show.vue?vue&type=template&id=c283d45a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_c283d45a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_c283d45a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/Shared/Berita.vue":
/*!****************************************!*\
  !*** ./resources/js/Shared/Berita.vue ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Berita_vue_vue_type_template_id_1ad81c83___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Berita.vue?vue&type=template&id=1ad81c83& */ "./resources/js/Shared/Berita.vue?vue&type=template&id=1ad81c83&");
/* harmony import */ var _Berita_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Berita.vue?vue&type=script&lang=js& */ "./resources/js/Shared/Berita.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Berita_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Berita_vue_vue_type_template_id_1ad81c83___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Berita_vue_vue_type_template_id_1ad81c83___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Shared/Berita.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Shared/Berita.vue?vue&type=script&lang=js&":
/*!*****************************************************************!*\
  !*** ./resources/js/Shared/Berita.vue?vue&type=script&lang=js& ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Berita_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Berita.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Berita.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Berita_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Shared/Berita.vue?vue&type=template&id=1ad81c83&":
/*!***********************************************************************!*\
  !*** ./resources/js/Shared/Berita.vue?vue&type=template&id=1ad81c83& ***!
  \***********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Berita_vue_vue_type_template_id_1ad81c83___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Berita.vue?vue&type=template&id=1ad81c83& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Berita.vue?vue&type=template&id=1ad81c83&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Berita_vue_vue_type_template_id_1ad81c83___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Berita_vue_vue_type_template_id_1ad81c83___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);