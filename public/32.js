(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[32],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Front/Organisasi.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Front/Organisasi.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../Shared/Layout */ "./resources/js/Shared/Layout.vue");
/* harmony import */ var vue_orgchart__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-orgchart */ "./node_modules/vue-orgchart/dist/vue-orgchart.min.js");
/* harmony import */ var vue_orgchart__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_orgchart__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue_organization_chart__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-organization-chart */ "./node_modules/vue-organization-chart/dist/orgchart.umd.min.js");
/* harmony import */ var vue_organization_chart__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue_organization_chart__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var vue_organization_chart_dist_orgchart_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-organization-chart/dist/orgchart.css */ "./node_modules/vue-organization-chart/dist/orgchart.css");
/* harmony import */ var vue_organization_chart_dist_orgchart_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue_organization_chart_dist_orgchart_css__WEBPACK_IMPORTED_MODULE_3__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Layout: _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__["default"],
    VoBasic: vue_orgchart__WEBPACK_IMPORTED_MODULE_1__["VoBasic"],
    OrganizationChart: vue_organization_chart__WEBPACK_IMPORTED_MODULE_2___default.a
  },
  props: ['title', 'data'],
  created: function created() {
    this.baru = this.settings();
  },
  methods: {
    settings: function settings() {
      var datas = this.data.map(function (el, key) {
        var chid1 = el.bawahans.map(function (el1, key1) {
          var chid2 = el1.bawahans.map(function (el2, key2) {
            return {
              name: el2.struktur_organisasi,
              id: el2.id_struktur_organisasi,
              title: el2.orang ? el2.orang.orang.nama : ''
            };
          });
          return {
            name: el1.struktur_organisasi,
            id: el1.id_struktur_organisasi,
            title: el1.orang ? el1.orang.orang.nama : '',
            children: chid2
          };
        });
        return {
          name: el.struktur_organisasi,
          id: el.id_struktur_organisasi,
          title: el.orang ? el.orang.orang.nama : '',
          children: chid1
        };
      });
      return datas[0];
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Front/Organisasi.vue?vue&type=template&id=7025a60b&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Front/Organisasi.vue?vue&type=template&id=7025a60b& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("Layout", { attrs: { title: _vm.title } }, [
    _c("div", { staticClass: "block block-border-bottom-grey block-pd-sm" }, [
      _c("div", { staticClass: "page-header" }, [
        _c("h2", { staticClass: "block-title" }, [_vm._v(_vm._s(_vm.title))])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12 " }, [
        _c(
          "div",
          { staticClass: "block block-border-bottom-grey block-pd-sm" },
          [
            _c("organization-chart", {
              attrs: {
                datasource: _vm.baru,
                pan: true,
                zoom: true,
                "zoomin-limit": 7,
                "zoomout-limit": 0.5
              }
            })
          ],
          1
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "span12", attrs: { id: "sp-breadcrumb" } }, [
      _c("ul", { staticClass: "breadcrumb" }, [
        _c(
          "li",
          [_c("inertia-link", { attrs: { href: "/" } }, [_vm._v("Home")])],
          1
        ),
        _vm._v(" "),
        _c("li", [_c("span", [_vm._v(_vm._s(_vm.title))])])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/Pages/Front/Organisasi.vue":
/*!*************************************************!*\
  !*** ./resources/js/Pages/Front/Organisasi.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Organisasi_vue_vue_type_template_id_7025a60b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Organisasi.vue?vue&type=template&id=7025a60b& */ "./resources/js/Pages/Front/Organisasi.vue?vue&type=template&id=7025a60b&");
/* harmony import */ var _Organisasi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Organisasi.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Front/Organisasi.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Organisasi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Organisasi_vue_vue_type_template_id_7025a60b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Organisasi_vue_vue_type_template_id_7025a60b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Front/Organisasi.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Front/Organisasi.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/Pages/Front/Organisasi.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Organisasi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Organisasi.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Front/Organisasi.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Organisasi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Front/Organisasi.vue?vue&type=template&id=7025a60b&":
/*!********************************************************************************!*\
  !*** ./resources/js/Pages/Front/Organisasi.vue?vue&type=template&id=7025a60b& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Organisasi_vue_vue_type_template_id_7025a60b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Organisasi.vue?vue&type=template&id=7025a60b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Front/Organisasi.vue?vue&type=template&id=7025a60b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Organisasi_vue_vue_type_template_id_7025a60b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Organisasi_vue_vue_type_template_id_7025a60b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);