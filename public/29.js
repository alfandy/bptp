(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[29],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Foto/Show.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Foto/Show.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../Shared/Layout */ "./resources/js/Shared/Layout.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['foto', 'domain', 'slug'],
  components: {
    Layout: _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Foto/Show.vue?vue&type=template&id=1d1551ef&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Foto/Show.vue?vue&type=template&id=1d1551ef& ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.foto
    ? _c("Layout", { attrs: { title: _vm.foto.name } }, [
        _c(
          "div",
          { staticClass: "block block-border-bottom-grey block-pd-sm" },
          [
            _c("div", { staticClass: "page-header" }, [
              _c("h2", { staticClass: "block-title" }, [
                _vm._v(_vm._s(_vm.foto.name))
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "entry-meta muted clearfix" }, [
              _c(
                "time",
                {
                  staticClass: "create-date",
                  attrs: { datetime: _vm.foto.created_at }
                },
                [
                  _vm._v(
                    "\n\t\t\t\tCreated: " +
                      _vm._s(_vm.$moment(_vm.foto.created_at).format("LLLL")) +
                      "\t\n\t\t\t"
                  )
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-12" }, [
              _c(
                "div",
                { staticClass: "block block-border-bottom-grey block-pd-sm" },
                [
                  _c("div", { staticClass: "col-sm-3" }),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-sm-6 text-center" }, [
                    _c(
                      "div",
                      {
                        staticClass: "carousel slide",
                        attrs: { id: "fotoShow", "data-ride": "carousel" }
                      },
                      [
                        _c(
                          "div",
                          { staticClass: "carousel-inner" },
                          _vm._l(JSON.parse(_vm.foto.image), function(
                            data,
                            key
                          ) {
                            return _c(
                              "div",
                              {
                                key: key,
                                staticClass: "item",
                                class: key == 0 ? "active" : ""
                              },
                              [
                                _c("img", {
                                  staticStyle: {
                                    "object-fit": "cover",
                                    width: "100%",
                                    height: "20em",
                                    "margin-left": "auto",
                                    "margin-right": "auto"
                                  },
                                  attrs: {
                                    src:
                                      _vm.domain +
                                      "/" +
                                      data.split(".")[0] +
                                      "." +
                                      data.split(".")[1],
                                    alt: _vm.foto.name
                                  }
                                })
                              ]
                            )
                          }),
                          0
                        ),
                        _vm._v(" "),
                        _c(
                          "a",
                          {
                            staticClass: "left carousel-control",
                            attrs: { href: "#fotoShow", "data-slide": "prev" }
                          },
                          [
                            _c("span", {
                              staticClass: "glyphicon glyphicon-chevron-left"
                            }),
                            _vm._v(" "),
                            _c("span", { staticClass: "sr-only" }, [
                              _vm._v("Previous")
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "a",
                          {
                            staticClass: "right carousel-control",
                            attrs: { href: "#fotoShow", "data-slide": "next" }
                          },
                          [
                            _c("span", {
                              staticClass: "glyphicon glyphicon-chevron-right"
                            }),
                            _vm._v(" "),
                            _c("span", { staticClass: "sr-only" }, [
                              _vm._v("Next")
                            ])
                          ]
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-sm-3" })
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "block block-border-bottom-grey block-pd-sm" },
                [
                  _c("p", {
                    domProps: { innerHTML: _vm._s(_vm.foto.deskripsi) }
                  })
                ]
              )
            ])
          ]
        ),
        _vm._v(" "),
        _c("div", { staticClass: "span12", attrs: { id: "sp-breadcrumb" } }, [
          _c("ul", { staticClass: "breadcrumb" }, [
            _c(
              "li",
              [_c("inertia-link", { attrs: { href: "/" } }, [_vm._v("Home")])],
              1
            ),
            _vm._v(" "),
            _c(
              "li",
              [
                _c("inertia-link", { attrs: { href: "/foto" } }, [
                  _vm._v(_vm._s(_vm.slug))
                ])
              ],
              1
            ),
            _vm._v(" "),
            _c("li", [_c("span", [_vm._v(_vm._s(_vm.foto.name))])])
          ])
        ])
      ])
    : _c("Layout", { attrs: { title: "Halaman Tidak Ditemukan" } }, [
        _c("h2", { staticClass: "text-center" }, [
          _c("b", [_vm._v("Halaman Tidak Ditemukan")])
        ])
      ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/Pages/Foto/Show.vue":
/*!******************************************!*\
  !*** ./resources/js/Pages/Foto/Show.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Show_vue_vue_type_template_id_1d1551ef___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Show.vue?vue&type=template&id=1d1551ef& */ "./resources/js/Pages/Foto/Show.vue?vue&type=template&id=1d1551ef&");
/* harmony import */ var _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Show.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Foto/Show.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Show_vue_vue_type_template_id_1d1551ef___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Show_vue_vue_type_template_id_1d1551ef___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Foto/Show.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Foto/Show.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/Pages/Foto/Show.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Show.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Foto/Show.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Foto/Show.vue?vue&type=template&id=1d1551ef&":
/*!*************************************************************************!*\
  !*** ./resources/js/Pages/Foto/Show.vue?vue&type=template&id=1d1551ef& ***!
  \*************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_1d1551ef___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Show.vue?vue&type=template&id=1d1551ef& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Foto/Show.vue?vue&type=template&id=1d1551ef&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_1d1551ef___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_1d1551ef___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);