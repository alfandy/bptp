(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[23],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Artikel/Index.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Artikel/Index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../Shared/Layout */ "./resources/js/Shared/Layout.vue");
/* harmony import */ var vue_carousel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-carousel */ "./node_modules/vue-carousel/dist/vue-carousel.min.js");
/* harmony import */ var vue_carousel__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_carousel__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // import carousel from 'vue-owl-carousel2'


/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['publikasis', 'domain', 'slug'],
  components: {
    Carousel: vue_carousel__WEBPACK_IMPORTED_MODULE_1__["Carousel"],
    Slide: vue_carousel__WEBPACK_IMPORTED_MODULE_1__["Slide"],
    Layout: _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      itemBerita: {
        paddingLeft: '4px',
        paddingRight: '4px'
      },
      imageBerita: {
        objectFit: 'cover',
        width: '277px',
        height: '152px',
        marginLeft: 'auto',
        marginRight: 'auto'
      },
      mfpDateAuthor: {
        fontSize: "80%",
        lineHeight: "120%",
        color: "#666",
        marginBottom: "10px",
        display: "block"
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Artikel/Index.vue?vue&type=template&id=076bf8b6&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Artikel/Index.vue?vue&type=template&id=076bf8b6& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("Layout", { attrs: { title: _vm.slug } }, [
    _c("div", [
      _c("div", { staticClass: "mission  block block-pd-sm block-bg-noise" }, [
        _c("h2", { staticClass: "container block-title" }, [
          _vm._v("\n\t\t\t\t" + _vm._s(_vm.slug) + "\n\t\t\t")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: " panel" }, [
          _c(
            "div",
            { staticClass: "panel-body" },
            _vm._l(_vm.publikasis.data, function(data, key) {
              return _c("div", { key: key, staticClass: "media" }, [
                _c("div", { staticClass: "media-left hidden-xs" }, [
                  _c("div", { staticClass: "date-wrapper" }, [
                    _c("span", { staticClass: "date-m" }, [
                      _vm._v(
                        _vm._s(_vm.$moment(data.waktu_publikasi).format("MMM"))
                      )
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "date-d" }, [
                      _vm._v(
                        _vm._s(_vm.$moment(data.waktu_publikasi).format("DD"))
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "media-body" }, [
                  _c("h4", { staticClass: "media-heading" }, [
                    _c(
                      "a",
                      {
                        staticClass: "text-weight-strong",
                        attrs: { href: data.url_publikasi }
                      },
                      [_vm._v(_vm._s(data.judul_publikasi))]
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "ul",
                    { staticClass: "list-inline meta text-muted visible-xs" },
                    [
                      _c("li", [
                        _c("i", { staticClass: "fa fa-calendar" }),
                        _vm._v(" "),
                        _c("span", { staticClass: "visible-md" }, [
                          _vm._v("Created:")
                        ]),
                        _vm._v(
                          " " +
                            _vm._s(
                              _vm
                                .$moment(data.waktu_publikasi)
                                .format("YYYY-MM-DD")
                            )
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("ul", { staticClass: "list-inline meta text-muted " }, [
                    data.authorr
                      ? _c("li", [
                          _c("i", { staticClass: "fa fa-user" }),
                          _vm._v(" "),
                          _c("a", { attrs: { href: data.url_publikasi } }, [
                            _vm._v(_vm._s(data.authorr.nama))
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("li", [
                      _c("a", { attrs: { href: data.url_publikasi } }, [
                        _vm._v(_vm._s(data.jenis_publikasi))
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("p", [
                    _vm._v(
                      "\n\t\t\t\t\t\t\t\t\tTempat : (" +
                        _vm._s(data.tempat_publikasi) +
                        ") , (" +
                        _vm._s(data.doi_publikasi) +
                        ") , Vol : (" +
                        _vm._s(data.vol_publikasi) +
                        ")\n\t\t\t\t\t\t\t\t\t"
                    ),
                    _c("a", { attrs: { href: data.url_publikasi } }, [
                      _vm._v("Read more "),
                      _c("i", { staticClass: "fa fa-angle-right" })
                    ])
                  ])
                ])
              ])
            }),
            0
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/Pages/Artikel/Index.vue":
/*!**********************************************!*\
  !*** ./resources/js/Pages/Artikel/Index.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Index_vue_vue_type_template_id_076bf8b6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Index.vue?vue&type=template&id=076bf8b6& */ "./resources/js/Pages/Artikel/Index.vue?vue&type=template&id=076bf8b6&");
/* harmony import */ var _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Index.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Artikel/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Index_vue_vue_type_template_id_076bf8b6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Index_vue_vue_type_template_id_076bf8b6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Artikel/Index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Artikel/Index.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/Pages/Artikel/Index.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Artikel/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Artikel/Index.vue?vue&type=template&id=076bf8b6&":
/*!*****************************************************************************!*\
  !*** ./resources/js/Pages/Artikel/Index.vue?vue&type=template&id=076bf8b6& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_076bf8b6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=template&id=076bf8b6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Artikel/Index.vue?vue&type=template&id=076bf8b6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_076bf8b6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_076bf8b6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);