(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[24],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Sdm/Index.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Sdm/Index.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../Shared/Layout */ "./resources/js/Shared/Layout.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Layout: _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: ['title', 'datas', 'domain']
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Sdm/Index.vue?vue&type=template&id=40d5d374&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Sdm/Index.vue?vue&type=template&id=40d5d374& ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("Layout", { attrs: { title: _vm.title } }, [
    _c("div", { staticClass: "block block-border-bottom-grey block-pd-sm" }, [
      _c("div", { staticClass: "page-header" }, [
        _c("h2", { staticClass: "block-title" }, [_vm._v(_vm._s(_vm.title))])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12" }, [
        _c(
          "div",
          { staticClass: "block block-border-bottom-grey block-pd-sm" },
          [
            _c(
              "div",
              { staticClass: "row" },
              _vm._l(_vm.datas, function(data, key) {
                return _c(
                  "div",
                  { key: key, staticClass: "col-xs-12 col-sm-6 col-md-4" },
                  [
                    _c("div", { staticClass: "well well-sm" }, [
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col-sm-6 col-md-4" }, [
                          data.orang && data.orang
                            ? _c("img", {
                                staticClass: "img-circle",
                                attrs: {
                                  src:
                                    _vm.domain + "/" + data.orang.photo_profil,
                                  name: data.orang.nama,
                                  width: "140",
                                  height: "140",
                                  border: "0"
                                }
                              })
                            : _vm._e()
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-sm-6 col-md-8" }, [
                          _c(
                            "h4",
                            [
                              _c(
                                "inertia-link",
                                { attrs: { href: "/sdm/" + data.id } },
                                [_vm._v(_vm._s(data.orang.nama))]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("small", [
                            data.orang.alamat
                              ? _c(
                                  "cite",
                                  { attrs: { title: data.orang.alamat } },
                                  [
                                    _vm._v(
                                      " " +
                                        _vm._s(
                                          data.orang.alamat
                                            ? data.orang.alamat
                                            : ""
                                        ) +
                                        "\n\t\t\t\t\t\t\t\t\t\t\t"
                                    ),
                                    _c("i", {
                                      staticClass:
                                        "glyphicon glyphicon-map-marker"
                                    })
                                  ]
                                )
                              : _vm._e()
                          ]),
                          _vm._v(" "),
                          _c("p"),
                          _c("div", [
                            _c("i", {
                              staticClass: "glyphicon glyphicon-envelope"
                            }),
                            _vm._v(
                              " " +
                                _vm._s(data.orang.user.email) +
                                "\n\t\t\t\t\t\t\t\t\t\t\t"
                            ),
                            _c("br")
                          ]),
                          _vm._v(" "),
                          data.orang.no_hp != "0"
                            ? _c("div", [
                                _c("i", {
                                  staticClass: "glyphicon glyphicon-phone"
                                }),
                                _vm._v(
                                  _vm._s(data.orang.no_hp) +
                                    "\n\t\t\t\t\t\t\t\t\t\t\t"
                                ),
                                _c("br")
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _c("i", { staticClass: "glyphicon glyphicon-globe" }),
                          _vm._v(
                            " " +
                              _vm._s(data.jabatan.jabatan) +
                              "\n\t\t\t\t\t\t\t\t\t"
                          ),
                          _c("p")
                        ])
                      ])
                    ])
                  ]
                )
              }),
              0
            )
          ]
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "span12", attrs: { id: "sp-breadcrumb" } }, [
      _c("ul", { staticClass: "breadcrumb" }, [
        _c(
          "li",
          [_c("inertia-link", { attrs: { href: "/" } }, [_vm._v("Home")])],
          1
        ),
        _vm._v(" "),
        _c("li", [_c("span", [_vm._v(_vm._s(_vm.title))])])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/Pages/Sdm/Index.vue":
/*!******************************************!*\
  !*** ./resources/js/Pages/Sdm/Index.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Index_vue_vue_type_template_id_40d5d374___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Index.vue?vue&type=template&id=40d5d374& */ "./resources/js/Pages/Sdm/Index.vue?vue&type=template&id=40d5d374&");
/* harmony import */ var _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Index.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Sdm/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Index_vue_vue_type_template_id_40d5d374___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Index_vue_vue_type_template_id_40d5d374___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Sdm/Index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Sdm/Index.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/Pages/Sdm/Index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Sdm/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Sdm/Index.vue?vue&type=template&id=40d5d374&":
/*!*************************************************************************!*\
  !*** ./resources/js/Pages/Sdm/Index.vue?vue&type=template&id=40d5d374& ***!
  \*************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_40d5d374___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=template&id=40d5d374& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Sdm/Index.vue?vue&type=template&id=40d5d374&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_40d5d374___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_40d5d374___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);