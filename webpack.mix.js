const mix = require('laravel-mix');
// const path = require('path');


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//  mix.babelConfig({
//     plugins: ["@babel/plugin-syntax-dynamic-import"]
// });

 mix.js(['resources/js/app.js'], 'public/js')
 .sass('resources/sass/app.scss', 'public/css');
 // mix.extract(['vue']); 
 mix.styles([
 	'public/assets/css/style.css',
 	'public/assets/css/table.css',
 	'public/assets/css/slider2.css',
    ] , 'public/assets/css/mix.css');
 // mix.webpackConfig({
 //    output: { chunkFilename: 'js/[name].js?id=[chunkhash]' },
 //    resolve: {
 //      alias: {
 //        vue$: 'vue/dist/vue.runtime.esm.js',
 //        '@': path.resolve('resources/js'),
 //      },
 //    },
 //  });
// mix.browserSync({
//     proxy: 'bptp.test'
// });

mix.options({
        hmrOptions: {
            host: 'bptp.test',  // site's host name 
            port: 8080,
        }
    });

    // // fix css files 404 issue
    mix.webpackConfig({
        // add any webpack dev server config here
        devServer: { 
            proxy: {
                host: '192.168.10.10',  // host machine ip
                port: 8080,
            },
            watchOptions:{
                aggregateTimeout:200,
                poll:5000
            },

        }
    });
 mix.version();

