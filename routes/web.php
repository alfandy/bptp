<?php

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Foto;
use App\Struktur;
use App\Organisasi;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('front.index');
Route::get('/post/{slug}', 'FrontController@showPost')->name('front.showPost');
Route::get('/post', 'FrontController@indexPost')->name('front.indexPost');
Route::get('/post/{slug}/categorie', 'FrontController@categoriePost')->name('front.categoriePost');
Route::get('/settings', 'SettingController@index')->name('setting.index');
Route::get('/pimpinan-kami', 'FrontController@pimpinanKamiIndex')->name('front.pimpinanKamiIndex');
Route::get('/sejarah', 'FrontController@sejarahSeting')->name('front.sejarahSeting');
Route::get('/organisasi', 'FrontController@organisasiIndex')->name('front.organisasiIndex');
Route::get('/sdm', 'FrontController@sdmIndex')->name('front.sdmIndex');
Route::get('/sdm/{id}', 'FrontController@sdmShow')->name('front.sdmShow');
Route::get('/visi-misi', 'FrontController@visiMisiIndex')->name('front.visiMisiIndex');
Route::get('/caffe-inovasi', 'FrontController@caveInovasiIndex')->name('front.caveInovasiIndex');
Route::get('/page/{slug}','FrontController@pageShow')->name('front.pageShow');
Route::get('/layanan-{id}','FrontController@layanaIndex')->name('front.layanaIndex');
Route::get('/layanan-{id}/{slug}','FrontController@layanaShow')->name('front.layanaShow');
Route::get('/foto','FrontController@fotoIndex')->name('front.fotoIndex');
Route::get('/foto/{id}','FrontController@fotoShow')->name('front.fotoShow');
Route::get('/video','FrontController@videoIndex')->name('front.videoIndex');
Route::get('/artikel','FrontController@artikelIndex')->name('front.artikelIndex');
// Route::get('/tes', function() {
//     return Foto::limit(5)->get();
// });



Route::get('/tes', function() {
    return Organisasi::with([
            'orang' => function($q){
                return $q->with([
                    'user' => function($q){
                        return $q->select('nik' , 'email');
                    },
                    'kepakaran' => function($q){
                        return $q->with(['kepakaran']);
                    }
                ]);
            },
            'jabatan' => function($q){
                return $q->select('id_jabatan' , 'jabatan');
            }
        ])
        ->orderBy('id_struktur_organisasi' , 'asc')
        ->orderBy('id_jabatan' , 'asc')
        ->get();
});

/*Route::get('/berita', function() {
	$url = '';
    return Categorie::with([
    	'posts' => function($q) use ($url){
    		return $q->select('*',DB::raw("CONCAT('$url/', image ) AS storege"))
    		->with(['author' ,'categorie'])
    		->where('status' ,'PUBLISHED')
    		->orderBy('created_at', 'desc')
    		// ->get();
    		->paginate(6);
    	}
    ])->get();
});


Route::get('/tes', function() {
    return Struktur::where('parent_organisasi','=', '0')
    	->with([
    		'orang' => function($q)
    			{
    				return $q->with(['orang']);
    			},
    		'bawahans' => function ($q)
	    	{
	    		return $q->with([
	    			'orang' => function($q)
	    			{
	    				return $q->with(['orang']);
	    			},
	    			'bawahans'
	    		])->orderby('id_struktur_organisasi', 'asc');
	    	}
	    ])
	    ->orderby('id_struktur_organisasi', 'asc')
    	->get();
});*/
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
