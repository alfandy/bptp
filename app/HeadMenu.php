<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeadMenu extends Model
{
    public function pages()
    {
    	return $this->hasMany('App\Page' , 'head_menu_id' , 'id');
    }
}
