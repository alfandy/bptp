<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organisasi extends Model
{
    protected $table = 't_organisasi';
    protected $guarded = ['id_struktur_organisasi'];

    // protected $primaryKey = ['id_struktur_organisasi','id_periode','id_jabatan'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    // public $incrementing = false;

    public function orang()
    {
    	return $this->belongsTo('App\Biodata' , 'nik' ,'nik');
    }

    public function jabatan()
    {
        return $this->belongsTo('App\Jabatan' , 'id_jabatan' ,'id_jabatan');
    }

    // public static function boot()
    // {
    // 	parent::boot();
    // 	self::creating(function ($model) {
    // 		$class = new Struktur;
    // 		$model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    // 	});
    // }
}
