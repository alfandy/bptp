<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTabel extends Model
{
    protected $table = 't_user';
    // protected $guarded = ['*'];
    protected $fillable = ['user','status','password','id_level','kd_user'];
    protected $primaryKey = 'kd_user';
    public $incrementing = false;
    public $timestamps = false;

    /*public static function boot()
    {
    	parent::boot();
    	self::creating(function ($model) {
    		$class = new UserTabel;
    		// $model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    		$model[$class->primaryKey] = Str::uuid();
    	});
    }*/
}
