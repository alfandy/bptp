<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{
    protected $table = 't_pekerjaan';
    protected $guarded = ['id_pekerjaan'];

    protected $primaryKey = 'id_pekerjaan';
    // public $incrementing = false;
    public $timestamps = false;

    public static function boot()
    {
    	parent::boot();
    	self::creating(function ($model) {
    		$class = new Pekerjaan;
    		$model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    		// $model[$class->primaryKey] = Str::uuid();
    	});
    }
}
