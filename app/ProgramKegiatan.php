<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ProgramKegiatan extends Model
{
    protected $table = 't_program_kegiatan';
    protected $guarded = ['kd_program'];

    protected $primaryKey = 'kd_program';
    public $incrementing = false;
    public $timestamps = false;

    public static function boot()
    {
    	parent::boot();
    	self::creating(function ($model) {
    		$class = new ProgramKegiatan;
    		// $model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    		$model[$class->primaryKey] = Str::uuid();
    	});
    }
}
