<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Struktur extends Model
{
    protected $table = 't_struktur_organisasi';
    protected $guarded = ['id_struktur_organisasi'];

    protected $primaryKey = 'id_struktur_organisasi';
    public $timestamps = false;

    public static function boot()
    {
    	parent::boot();
    	self::creating(function ($model) {
    		$class = new Struktur;
    		$model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    	});
    }

    public function bawahans()
    {
    	return $this->hasMany('App\Struktur','parent_organisasi','id_struktur_organisasi');
    }

    public function orang()
    {
    	return $this->belongsTo('App\Organisasi','id_struktur_organisasi','id_struktur_organisasi');
    }
    public function orangs()
    {
    	return $this->hasMany('App\Organisasi','id_struktur_organisasi','id_struktur_organisasi');
    }
}
