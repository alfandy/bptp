<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    protected $table = 't_biodata';
    protected $fillable = ['nik','nama','jenis_kelamin','tempat_lahir','tanggal_lahir','id_agama','golongan_darah','no_hp','alamat','photo_profil'];

    protected $primaryKey = 'nik';
    public $timestamps = false;

    // public static function boot()
    // {
    // 	parent::boot();
    // 	self::creating(function ($model) {
    // 		$class = new Teknologi;
    // 		$model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    // 	});
    // }
    
    public function user()
    {
        return $this->belongsTo('App\User' , 'nik' , 'nik');
    }

    public function kepakaran()
    {
        return $this->belongsTo('App\TransaksiKepakaran' , 'nik' ,'nik');
    }
}
