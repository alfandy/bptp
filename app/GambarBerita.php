<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GambarBerita extends Model
{
    protected $table = 't_gambar_berita';
    // protected $guarded = ['*'];
    protected $fillable = ['kd_berita','url_gambar','waktu_upload'];
    protected $primaryKey = 'kd_berita';
    public $incrementing = false;
    public $timestamps = false;
}
