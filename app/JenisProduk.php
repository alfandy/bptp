<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisProduk extends Model
{
    protected $table = 't_jenis_produk';
    protected $guarded = ['id_jenis_produk'];

    protected $primaryKey = 'id_jenis_produk';
    // public $incrementing = false;
    public $timestamps = false;

    public static function boot()
    {
    	parent::boot();
    	self::creating(function ($model) {
    		$class = new JenisProduk;
    		$model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    		// $model[$class->primaryKey] = Str::uuid();
    	});
    }
}
