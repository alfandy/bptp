<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teknologi extends Model
{
    protected $table = 'm_teknologi';
    protected $guarded = ['id_teknologi'];

    protected $primaryKey = 'id_teknologi';
    public $timestamps = false;

    public static function boot()
    {
    	parent::boot();
    	self::creating(function ($model) {
    		$class = new Teknologi;
    		$model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    	});
    }
}
