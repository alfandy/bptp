<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Publikasi extends Model
{
    protected $table = 't_publikasi';
    protected $guarded = ['kd_publikasi'];

    protected $primaryKey = 'kd_publikasi';
    public $incrementing = false;
    public $timestamps = false;
    // const CREATED_AT = 'waktu_publikasi';
    // const UPDATED_AT = 'waktu_publikasi';

    public static function boot()
    {
    	parent::boot();
    	self::creating(function ($model) {
    		$class = new Publikasi;
    		// $model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    		$model[$class->primaryKey] = Str::uuid();
    	});
    }

    public function authorr()
    {
    	return $this->belongsTo('App\Biodata'  , 'author','nik');
    }
}
