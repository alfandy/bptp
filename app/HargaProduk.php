<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class HargaProduk extends Model
{
    protected $table = 't_harga_produk';
    protected $guarded = ['kd_harga_produk'];

    protected $primaryKey = 'kd_harga_produk';
    public $incrementing = false;
    public $timestamps = false;

    public static function boot()
    {
    	parent::boot();
    	self::creating(function ($model) {
    		$class = new HargaProduk;
    		// $model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    		$model[$class->primaryKey] = Str::uuid();
    	});
    }
}
