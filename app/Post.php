<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function author()
    {
    	return $this->belongsTo('App\User', 'author_id', 'id');
    }

    public function categorie()
    {
    	return $this->belongsTo('App\Categorie', 'category_id', 'id');
    }
}
