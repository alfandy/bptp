<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use Inertia\Inertia;
use TCG\Voyager\Facades\Voyager;
use App\HeadMenu;
use App\Statistik;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        $useragent =  $request->server('HTTP_USER_AGENT');
        // "<br/>";
        $ip = $request->ip();
        // "<br/>";
        $date = date('Y-m-d');
        // die();
        Statistik::updateOrCreate([
            'ip' => $ip,
            'useragent' => $useragent,
            'date' => $date
        ]);

        // Inertia::version($version);

              // If you're using Laravel Mix, you can
              // use the mix-manifest.json for this.
        Inertia::version(function () {
            return md5_file(public_path('mix-manifest.json'));
        });

        Inertia::share([
            // Synchronously
            'app' => [
                'title' => Voyager::setting('site.title'),
                'description' => Voyager::setting('site.description'),
                'logo' => url('storage').'/'.Voyager::setting('site.logo'),
                'alamat' => Voyager::setting('site.alamat'),
                'twitter' => Voyager::setting('site.twitter'),
                'fb' => Voyager::setting('site.fb'),
                'ig' => Voyager::setting('site.ig'),
                'gplus' => Voyager::setting('site.gplus'),
                'noTelepon' => setting('site.noTelepon'),
                'email' => setting('site.email'),
                'noFax' => setting('site.noFax'),
                'domain' => url('storage'),
                'headMenus' => HeadMenu::with([
                            'pages' => function($q)
                                {
                                    return $q->select('head_menu_id','slug','title','updated_at' ,'order' , 'route')
                                                ->where('status','ACTIVE')
                                                ->orderBy('order', 'asc')
                                                ->orderBy('updated_at', 'desc');
                                }
                            ])
                            ->orderBy('order','asc')->orderBy('updated_at','desc')->get(),
            ]

            // Lazily
            /*'auth' => function () {
                return [
                    'user' => Auth::user() ? [
                        'id' => Auth::user()->id,
                        'first_name' => Auth::user()->first_name,
                        'last_name' => Auth::user()->last_name,
                    ] : null
                ];
            }*/
        ]);
    }
}
