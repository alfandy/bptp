<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kampus extends Model
{
    protected $table = 't_kampus';
    protected $guarded = ['id_kampus'];

    protected $primaryKey = 'id_kampus';
    // public $incrementing = false;
    public $timestamps = false;

    public static function boot()
    {
    	parent::boot();
    	self::creating(function ($model) {
    		$class = new Kampus;
    		$model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    		// $model[$class->primaryKey] = Str::uuid();
    	});
    }
}
