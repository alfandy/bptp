<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kepakaran extends Model
{
    protected $table = 'm_kepakaran';
    protected $guarded = ['id_kepakaran'];

    protected $primaryKey = 'id_kepakaran';
    // public $incrementing = false;
    public $timestamps = false;

    public static function boot()
    {
    	parent::boot();
    	self::creating(function ($model) {
    		$class = new Kepakaran;
    		$model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    		// $model[$class->primaryKey] = Str::uuid();
    	});
    }
}
