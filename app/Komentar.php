<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 't_komentar';
    protected $guarded = ['kd_komentar'];
}
