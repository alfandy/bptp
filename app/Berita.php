<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = 't_berita';
    // protected $guarded = ['*'];
    protected $fillable = ['kd_berita','judul_berita','status','waktu','konten_berita','tipe'];
    protected $primaryKey = 'kd_berita';
    public $incrementing = false;
    public $timestamps = false;
}
