<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agama extends Model
{
    protected $table = 'm_agama';
    protected $guarded = ['id_agama'];
}
