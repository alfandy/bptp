<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoriPendidikan extends Model
{
    protected $table = 't_histori_pendidikan';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    // public $incrementing = false;
    public $timestamps = false;

    // public static function boot()
    // {
    // 	parent::boot();
    // 	self::creating(function ($model) {
    // 		$class = new HistoriPendidikan;
    // 		// $model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    // 		$model[$class->primaryKey] = Str::uuid();
    // 	});
    // }
}
