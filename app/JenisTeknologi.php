<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class JenisTeknologi extends Model
{
    protected $table = 't_jenis_teknologi';
    protected $guarded = ['kd_jenis_teknologi'];

    protected $primaryKey = 'kd_jenis_teknologi';
    public $incrementing = false;
    public $timestamps = false;

    public static function boot()
    {
    	parent::boot();
    	self::creating(function ($model) {
    		$class = new JenisTeknologi;
    		// $model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    		$model[$class->primaryKey] = Str::uuid();
    	});
    }
}
