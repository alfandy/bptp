<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public function author()
    {
    	return $this->belongsTo('App\User', 'author_id', 'id');
    }

    public function head_menu()
    {
    	return $this->belongsTo('App\HeadMenu', 'head_menu_id', 'id');
    }

}
