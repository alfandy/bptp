<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;
use TCG\Voyager\Facades\Voyager;
use App\Post;
use App\Categorie;
use App\VisiMisi;
use App\Page;
use App\Layanan;
use App\Struktur;
use App\Foto;
use App\Videoyutube;
use App\Publikasi;
use App\Statistik;
use App\Organisasi;


class FrontController extends Controller
{
    public function index()
    {
    	$url = "";
    	$domain = url('storage');
		// $category = Categorie::get();    	

		$data = [
			// 'posts' => Post::select('*',DB::raw("CONCAT('$url/', image ) AS storege"))
			// 			->with(['author' ,'categorie'])
			// 			->where('status' ,'PUBLISHED')
			// 			->orderBy('created_at', 'desc')
			// 			// ->get();
			// 			->paginate(6),
			'title' => 'BPTP Provinsi Gorontalo',
			'deskripsi' => 'Selamat Datang Di Website Resmi BPTP Provinsi Gorontalo',
			'logo' => url('storage').'/'.Voyager::setting('site.logo'),
			'category' =>  Categorie::with([
					'posts' => function($q) use ($url){
						return $q->select('*',DB::raw("CONCAT('$url/', image ) AS storege"))
						->with(['author' ,'categorie'])
						->where('status' ,'PUBLISHED')
						->orderBy('created_at', 'desc')
						->limit(6);
					}
				])
				->orderBy('order' , 'asc')
				->limit(5)
				->get(),
			'fotos' => Foto::limit(5)->get(),
			'video' =>  Videoyutube::orderBy('created_at' , 'desc')
						->first(),
			'publikasis' => Publikasi::with(['authorr'])->limit(3)->orderBy('waktu_publikasi','desc')->orderBy('waktu_submit')->get(),
			'statik' => [
				'hariIni' => Statistik::whereRaw('date = CURDATE()')->get()->count(),
				'kemarin' => Statistik::whereRaw("date = SUBDATE(CURDATE(), 1)")->get()->count(),
				'mingguIni' => Statistik::whereRaw("YEARWEEK(date) = YEARWEEK(CURDATE())")->get()->count(),
				'bulanIni' => Statistik::whereRaw("YEAR(date) = YEAR(CURDATE()) AND MONTH(date) = MONTH(CURDATE())")->get()->count(),
				'semua' => Statistik::get()->count()
			],
			'domain' => $domain
		];
        return Inertia::render('Front/Index',$data);
    }

    public function indexPost()
    {
    	$url = "";
    	$domain = url('storage');

		$data = [
			'posts' => Post::select('*',DB::raw("CONCAT('$url/', image ) AS storege"))
						->with(['author','categorie'])
						->where('status' ,'PUBLISHED')
						->orderBy('created_at', 'desc')
						// ->get();
						->paginate(6),
			'domain' => $domain
		];
        return Inertia::render('Post/Index',$data);
    }

    public function categoriePost($slug)
    {
    	$where =  Categorie::where('slug' , $slug)->first();

    	$url = "";
    	$domain = url('storage');


		$data = [
			'posts' => Post::select('*',DB::raw("CONCAT('$url/', image ) AS storege"))
						->with(['author','categorie'])
						->where('status' ,'PUBLISHED')
						->where('category_id' , $where->id)
						->orderBy('created_at', 'desc')
						// ->get();
						->paginate(6),
			'domain' => $domain,
			'slug' => ucfirst(str_replace('-', ' ', $slug))
		];
        return Inertia::render('Post/Categorie',$data);
    }

    public function showPost($slug)
    {
    	$url = "";
		$domain = url('storage');
		$data =[
			'post' =>  Post::select('*',DB::raw("CONCAT('$url/', image ) AS storege"))
						->with(['author' ,'categorie'])
						->where('status' ,'PUBLISHED')
						->where('slug' , $slug)
						->first() ,
			'domain' => $domain,
			'posts' => Post::select('*',DB::raw("CONCAT('$url/', image ) AS storege"))
						->with(['author','categorie'])
						->where('status' ,'PUBLISHED')
						->orderBy('created_at', 'desc')
						// ->get();
						->paginate(6),
			'title' => 'BPTP Provinsi Gorontalo'
		];
    	return Inertia::render('Post/Show',$data);	
    }

    public function sejarahSeting()
    {
    	
		$data =[
			'title' =>  'Sejarah',
			'data' => Voyager::setting('site.sejarah'),
		];
    	return Inertia::render('Front/Sejarah',$data);	
    }

    public function visiMisiIndex()
    {
    	$tanggal = date('Y-m-d');
		$data =[
			'title' =>  'Visi dan Misi',
			'data' => VisiMisi::where('tmt_visimisi', '<=' ,$tanggal)
						->where('tst_visimisi' , '>=',$tanggal)
						->first()
		];
    	return Inertia::render('Front/VisiMisi',$data);	
    }

    public function caveInovasiIndex()
    {
		$data =[
			'title' =>  'Caffe Inovasi',
			'data' => ''
		];
    	return Inertia::render('Front/CaveInovasi',$data);	
    }

    public function organisasiIndex()
    {
		$data =[
			'title' =>  'Organisasi',
			'data' => Struktur::where('parent_organisasi','=', '0')
			    	->with([
			    		'orang' => function($q)
			    			{
			    				return $q->with(['orang']);
			    			},
			    		'bawahans' => function ($q)
				    	{
				    		return $q->with([
				    			'orang' => function($q)
				    			{
				    				return $q->with(['orang']);
				    			},
				    			'bawahans'
				    		])->orderby('id_struktur_organisasi', 'asc');
				    	}
				    ])
				    ->orderby('id_struktur_organisasi', 'asc')
			    	->get()
		];
    	return Inertia::render('Front/Organisasi',$data);	
    }

    public function sdmIndex()
    {
    	$domain = url('storage');
		$data =[
			'title' =>  'SDM (Sumber daya Manusia)',
			'datas' => Organisasi::with([
			            'orang' => function($q){
			                return $q->with([
			                    'user' => function($q){
			                        return $q->select('nik' , 'email');
			                    },
			                    'kepakaran' => function($q){
			                        return $q->with(['kepakaran']);
			                    }
			                ]);
			            },
			            'jabatan' => function($q){
			                return $q->select('id_jabatan' , 'jabatan');
			            }
			        ])
			        ->orderBy('id_struktur_organisasi' , 'asc')
			        ->orderBy('id_jabatan' , 'asc')
			        ->get(),
			'domain' =>  $domain
		];
    	return Inertia::render('Sdm/Index',$data);	
    }

    public function sdmShow($id)
    {
    	$domain = url('storage');
		$data =[
			'title' =>  'SDM (Sumber daya Manusia)',
			'data' => Organisasi::where('id' , $id)
					->with([
			            'orang' => function($q){
			                return $q->with([
			                    'user' => function($q){
			                        return $q->select('nik' , 'email');
			                    },
			                    'kepakaran' => function($q){
			                        return $q->with(['kepakaran']);
			                    }
			                ]);
			            },
			            'jabatan' => function($q){
			                return $q->select('id_jabatan' , 'jabatan');
			            }
			        ])
			        ->orderBy('id_struktur_organisasi' , 'asc')
			        ->orderBy('id_jabatan' , 'asc')
			        ->first(),
			'domain' =>  $domain
		];
    	return Inertia::render('Sdm/Show',$data);
    }

    public function pimpinanKamiIndex()
    {
    	$domain = url('storage');
		$data =[
			'title' =>  'Pimpinan Kami',
			'data' => Struktur::where('parent_organisasi' , '0')
				        ->with([
				            'orang' => function($q){
				                return $q->orderBy('id_periode' , 'desc')->with([
				                    'orang' => function($q){
				                        return $q->with([
				                            'user' => function($q){
				                                return $q->select('nik','email');
				                            }
				                        ]);
				                    },
				                    'jabatan' => function($q)
				                    {
				                        return $q->select('id_jabatan' ,'jabatan');
				                    }
				                ]);
				            }
				        ])
				        ->first(),
			'domain' =>  $domain
		];
    	return Inertia::render('Front/PimpinanKami',$data);	
    }

    public function pageShow($slug)
    {
    	$url = "";
		$domain = url('storage');
		$data =[
			'page' =>  Page::select('*',DB::raw("CONCAT('$url/', image ) AS storege"))
						->with(['author','head_menu'])
						->where('status' ,'ACTIVE')
						->where('slug' , $slug)
						->first() ,
			'domain' => $domain,
			'posts' => Post::select('*',DB::raw("CONCAT('$url/', image ) AS storege"))
						->with(['author','categorie'])
						->where('status' ,'PUBLISHED')
						->orderBy('created_at', 'desc')
						// ->get();
						->paginate(6),
		];
    	return Inertia::render('Page/Show',$data);
    }

    public function layanaIndex($id)
    {

    	$url = "";
    	$domain = url('storage');
    	switch ($id) {
    		case '2':
    			$slug = 'Pelatihan';
    			$i = '2';
    			break;
    		case '3':
    			$slug = 'Bimtek';
    			$i = '3';
    			break;
    		case '4':
    			$slug = 'Info Teknologi';
    			$i = '4';
    			break;
    		
    		default:
    			$slug = 'Praktek Kerja / Magang';
    			$i = '1';
    			break;
    	}

		$data = [
			'layanans' => Layanan::select('*',DB::raw("CONCAT('$url/', image ) AS storege"))
						->where('status' ,'ACTIVE')
						->where('category' , $i)
						->orderBy('created_at', 'desc')
						// ->get();
						->paginate(6),
			'domain' => $domain,
			'slug' => $slug
		];
        return Inertia::render('Layanan/index',$data);
    }


    public function layanaShow($id , $sluga)
    {
    	$url = "";
		$domain = url('storage');
		switch ($id) {
    		case '2':
    			$slug = 'Pelatihan';
    			$i = '2';
    			break;
    		case '3':
    			$slug = 'Bimtek';
    			$i = '3';
    			break;
    		case '4':
    			$slug = 'Info Teknologi';
    			$i = '4';
    			break;
    		
    		default:
    			$slug = 'Praktek Kerja / Magang';
    			$i = '1';
    			break;
    	}
		$data =[
			'layanan' =>  Layanan::select('*',DB::raw("CONCAT('$url/', image ) AS storege"))
						->where('status' ,'ACTIVE')
						->where('category' , $i)
						->where('slug' , $sluga)
						->first() ,
			'domain' => $domain,
			'slug' => $slug
		];
    	return Inertia::render('Layanan/Show',$data);	
    }

    public function fotoIndex()
    {
    	$url = "";
		$domain = url('storage');
		$data =[
			'fotos' =>  Foto::select('*',DB::raw("CONCAT('$url/', image ) AS storege"))
						->orderBy('created_at' , 'desc')
						->paginate(6),
			'slug' => 'Foto',

			'domain' => $domain
		];
    	return Inertia::render('Foto/Index',$data);	
    }

    public function  fotoShow($id)
    {
    	$url = "";
		$domain = url('storage');
		$data =[
			'foto' =>  $foto = Foto::select('*',DB::raw("CONCAT('$url/', image ) AS storege"))
						->where('id' ,$id)
						->first() ,
			'domain' => $domain,
			'slug' => 'Foto'
		];
    	return Inertia::render('Foto/Show',$data);	
    }

    public function videoIndex()
    {
    	$url = "";
		$domain = url('storage');
		$data =[
			'videos' =>  Videoyutube::select('*')
						->orderBy('created_at' , 'desc')
						->paginate(100),
			'slug' => 'Video',

			'domain' => $domain
		];
    	return Inertia::render('Video/Index',$data);	
    }

    public function artikelIndex()
    {
    	$url = "";
		$domain = url('storage');
		$data =[
			'publikasis' =>  Publikasi::with(['authorr'])->limit(3)->orderBy('waktu_publikasi','desc')->orderBy('waktu_submit')->paginate(100),
			'slug' => 'Artikel',

			'domain' => $domain
		];
    	return Inertia::render('Artikel/Index',$data);	
    }
}
