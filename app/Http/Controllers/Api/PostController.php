<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Post;
use App\Categorie;

class PostController extends Controller
{
	public function index()
	{
		// $url = "";
		$url = url('storage');

		$data = Post::select('*',DB::raw("CONCAT('$url/', image ) AS storege"))
						->with(['author' ,'categorie'])
						->where('status' ,'PUBLISHED')
						->orderBy('created_at', 'desc')
						// ->get();
						->paginate(6);
	    return $data;
	}
}
