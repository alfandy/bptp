<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AntarBalai extends Model
{
    protected $table = 't_antar_balai';
    protected $guarded = ['id'];

    protected $primaryKey = 'id';
    public $timestamps = false;

    public static function boot()
    {
    	parent::boot();
    	self::creating(function ($model) {
    		$class = new AntarBalai;
    		$model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    	});
    }
}
