<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model
{
	protected $table = 'm_ruangan';
	protected $guarded = ['id_ruangan'];
	protected $primaryKey = 'id_ruangan';
	public $timestamps = false;

	public static function boot()
	{
		parent::boot();
		self::creating(function ($model) {
			$class = new Ruangan;
			$model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
		});
	}
}
