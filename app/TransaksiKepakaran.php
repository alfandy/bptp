<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiKepakaran extends Model
{
    protected $table = 'tr_kepakaran';
    protected $guarded = ['nik'];

    protected $primaryKey = 'nik';
    public $incrementing = false;
    public $timestamps = false;

    /*public static function boot()
    {
    	parent::boot();
    	self::creating(function ($model) {
    		$class = new Transaksi;
    		// $model[$class->primaryKey] = $class->count() < 1 ? 1 : $class->orderBy($class->primaryKey, 'desc')->first()[$class->primaryKey] + 1;
    		$model[$class->primaryKey] = Str::uuid();
    	});
    }*/

    public function kepakaran()
    {
        return $this->belongsTo('App\Kepakaran' ,'id_kepakaran' , 'id_kepakaran');
    }
}
